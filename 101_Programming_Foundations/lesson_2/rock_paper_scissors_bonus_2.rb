load 'helper_methods.rb'

CHOICES = %w(rock paper scissors lizard spock)

WINNING_MOVES = { 'paper' => ['rock', 'spock'],
                  'rock' => ['scissors', 'lizard'],
                  'scissors' => ['paper', 'lizard'],
                  'spock' => ['scissors', 'rock'],
                  'lizard' => ['spock', 'paper'] }

def win?(first, second)
  WINNING_MOVES[first].include?(second)
end

def choice_convert_alias(input)
  case input
  when 'r'
    'rock'
  when 'p'
    'paper'
  when 'sc'
    'scissors'
  when 'l'
    'lizard'
  when 'sp'
    'spock'
  else
    return input
  end
end

def display_results(computer, player)
  if win?(player, computer)
    prompt 'You win!'
  elsif win?(computer, player)
    prompt 'I win!'
  else
    prompt 'It\'s a draw!'
  end
end

loop do
  choice = ''
  clear_screen
  loop do
    choice_prompt = <<-MSG.strip
      What's your move?
      rock (r)
      paper (p)
      scissors (sc)
      lizard (l)
      spock (sp)
    MSG

    prompt(choice_prompt)
    choice = gets.chomp

    choice = choice_convert_alias(choice)

    if CHOICES.include?(choice)
      break
    else
      prompt 'Entry is not valid. Please try again.'
    end
  end

  computer = CHOICES.sample

  prompt "You picked #{choice}. I picked #{computer}!"
  display_results(computer, choice)

  prompt 'Would you like to play again? (Y for yes)'
  answer = gets.chomp.upcase
  break unless answer.start_with?('Y')
end
