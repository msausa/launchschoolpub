#require 'pry'

def prompt(message)
  puts "=> #{message}"
end

def is_valid?(i)
  (i.to_i.to_s == i || i.to_f.to_s == i) && i.to_f > 0
end

loop do
  p = ''
  loop do
    prompt 'Please enter your loan amount:'
    p = gets.chomp

    if is_valid?(p)
      p = p.to_f
      break
    else
      prompt 'Please enter a valid loan amount'
    end
  end

  j = ''
  loop do
      prompt 'Please enter your APR:'
      j = gets.chomp

      if is_valid?(j)
        j = j.to_f / 1200
        break
      else
        prompt 'Please enter a valid APR (ex. 3.5)'
      end
    end

    n = ''
    loop do
          prompt 'Please enter the duration of the loan in years'
        n = gets.chomp

        if is_valid?(n)
          n = n.to_f * 12
          break
        else
          prompt 'Please enter a valid APR (ex. 3.5)'
        end
      end

  m = p * (j / (1 - (1 + j)**-n))
  m = m.round(2)

prompt "Your monthly payment will be #{m}"

prompt 'Would you like to make another calcuation? (Y for yes)'
answer = gets.chomp.upcase
break unless answer.start_with?('Y')
end
