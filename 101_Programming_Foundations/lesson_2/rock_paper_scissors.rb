load 'helper_methods.rb'

CHOICES = %w(rock paper scissors)

def win?(first, second)
  (first == 'paper' && second == 'rock') ||
    (first == 'rock' && second == 'scissors') ||
    (first == 'scissors' && second == 'paper')
end

def display_results(computer, player)
  if win?(player, computer)
    prompt 'You win!'
  elsif win?(computer, player)
    prompt 'I win!'
  else
    prompt 'It\'s a draw!'
  end
end

loop do
  choice = ''
  loop do
    prompt "Choose one: #{CHOICES.join(', ')}"
    choice = gets.chomp

    if CHOICES.include?(choice)
      break
    else
      prompt 'Entry is not valid. Please try again.'
    end
  end

  computer = CHOICES.sample

  prompt "You picked #{choice}. I picked #{computer}!"
  display_results(computer, choice)

  prompt 'Would you like to play again? (Y for yes)'
  answer = gets.chomp.upcase
  break unless answer.start_with?('Y')
end
