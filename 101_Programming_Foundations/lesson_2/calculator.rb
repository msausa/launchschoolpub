# require 'pry'

def prompt(message)
  puts "=> #{message}"
end

def is_num?(i)
  i.to_i.to_s == i || i.to_f.to_s == i
end

def valid_operation?(operation)
  possibilities = %w(1 2 3 4)
  possibilities.include?(operation)
end

loop do
  num1 = ''
  loop do
    prompt 'Enter your first number:'
    num1 = gets.chomp

    if is_num?(num1)
      num1 = num1.to_f
      break
    else
      prompt 'Please enter a valid number'
    end
  end

  num2 = ''
  loop do
    prompt 'Enter your second number:'
    num2 = gets.chomp

    if is_num?(num2)
      num2 = num2.to_f
      break
    else
      prompt 'Please enter a valid number'
    end
  end

  operator_prompt = <<-MSG
    Which operation would you like to perform?
    1) add
    2) subtract
    3) multiply
    4) divide
  MSG

  prompt(operator_prompt)

  operation = ''
  loop do
    operation = gets.chomp.downcase

    if valid_operation?(operation)
      break
    else
      puts 'Please enter a valid operation'
    end
  end

  result = case operation
           when '1'
             num1 + num2
           when '2'
             num1 - num2
           when '3'
             num1 * num2
           when '4'
             num1.to_f / num2.to_f
           end

  # binding.pry
  puts result

  prompt 'Do you want to calculate another number (Y for yes)'
  answer = gets.chomp.upcase
  break unless answer.start_with?('Y')
end
