# 1. Display the initial empty 3x3 board.
# 2. Ask the user to mark a square.
# 3. Computer marks a square.
# 4. Display the updated board state.
# 5. If winner, display winner.
# 6. If board is full, display tie.
# 7. If neither winner nor board is full, go to #2
# 8. Play again?
# 9. If yes, go to #1
# 10. Good bye!

# Set initial player_wins to 0
# Set initial computer_wins to 0
# At the end of a game, up the count of the winning player by 1
# Display "Computer wins: number of wins, Player wins: number of wins"
# Break if player wins or computer wins reach 5

load 'helper_methods.rb'
require 'pry'

INITIAL_MARKER = ' '.freeze
PLAYER_MARKER = 'X'.freeze
COMPUTER_MARKER = 'O'.freeze
board = {}

def display_board(brd)
  clear_screen
  puts ""
  puts "     |     |     "
  puts "  #{brd[1]}  |  #{brd[2]}  |  #{brd[3]}  "
  puts "     |     |     "
  puts "-----+-----+-----"
  puts "     |     |     "
  puts "  #{brd[4]}  |  #{brd[5]}  |  #{brd[6]}  "
  puts "     |     |     "
  puts "-----+-----+-----"
  puts "     |     |     "
  puts "  #{brd[7]}  |  #{brd[8]}  |  #{brd[9]}  "
  puts "     |     |     "
  puts ""
end

def initialize_board(brd)
  (1..9).each { |num| brd[num] = INITIAL_MARKER }
end

def empty_squares(brd)
  brd.keys.select { |num| brd[num] == INITIAL_MARKER }
end

def player_move!(brd)
  square = ' '
  loop do
    prompt "Please choose a square. Remaining squares = #{empty_squares(brd)}"
    square = gets.chomp.to_i

    break if empty_squares(brd).include?(square)
    prompt 'That is not a valid entry. Please try again.'
  end
  brd[square] = PLAYER_MARKER
end

def computer_move!(brd)
  square = empty_squares(brd).sample
  brd[square] = COMPUTER_MARKER
end

def someone_won?(brd)
  !!detect_winner(brd)
end

def detect_winner(brd)
  winning_lines = [[1, 2, 3], [4, 5, 6], [7, 8, 9]] + # rows
                  [[1, 4, 7], [2, 5, 8], [3, 6, 9]] + # columns
                  [[1, 5, 9], [3, 5, 7]] # diagonals
  winning_lines.each do |line|
    if brd[line[0]] == PLAYER_MARKER &&
       brd[line[1]] == PLAYER_MARKER &&
       brd[line[2]] == PLAYER_MARKER
      return 'Player'
    elsif brd[line[0]] == COMPUTER_MARKER &&
          brd[line[1]] == COMPUTER_MARKER &&
          brd[line[2]] == COMPUTER_MARKER
      return 'Computer'
    end
  end
  nil
end

player_wins = 0
computer_wins = 0
loop do
  initialize_board(board)

  loop do
    display_board(board)
    player_move!(board)
    break if someone_won?(board) || empty_squares(board).empty?

    computer_move!(board)
    break if someone_won?(board) || empty_squares(board).empty?
  end

  display_board(board)

  if someone_won?(board)
    prompt "#{detect_winner(board)} won!"
  else
    prompt "It's a tie!"
  end

  if detect_winner(board) == 'Player'
    player_wins += 1
  elsif detect_winner(board) == 'Computer'
    computer_wins +=1
  end
  puts "Player wins: #{player_wins}"
  puts "Computer wins: #{computer_wins}"

  if player_wins == 5 || computer_wins == 5
    prompt "#{detect_winner(board)} wins it all!"
    break
  else
    prompt 'Best of 5? (Y for yes)'
    play_again = gets.chomp.downcase

    break if play_again != 'y'
  end
end
