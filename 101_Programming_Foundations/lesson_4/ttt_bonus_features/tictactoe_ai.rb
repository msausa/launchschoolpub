load 'helper_methods.rb'
require 'pry'

INITIAL_MARKER = ' '.freeze
PLAYER_MARKER = 'X'.freeze
COMPUTER_MARKER = 'O'.freeze

WINNING_LINES = [[1, 2, 3], [4, 5, 6], [7, 8, 9]] + # rows
                [[1, 4, 7], [2, 5, 8], [3, 6, 9]] + # columns
                [[1, 5, 9], [3, 5, 7]] # diagonals
board = {}

def display_board(brd)
  clear_screen
  puts ""
  puts "     |     |     "
  puts "  #{brd[1]}  |  #{brd[2]}  |  #{brd[3]}  "
  puts "     |     |     "
  puts "-----+-----+-----"
  puts "     |     |     "
  puts "  #{brd[4]}  |  #{brd[5]}  |  #{brd[6]}  "
  puts "     |     |     "
  puts "-----+-----+-----"
  puts "     |     |     "
  puts "  #{brd[7]}  |  #{brd[8]}  |  #{brd[9]}  "
  puts "     |     |     "
  puts ""
end

def initialize_board(brd)
  (1..9).each { |num| brd[num] = INITIAL_MARKER }
end

def empty_squares(brd)
  brd.keys.select { |num| brd[num] == INITIAL_MARKER }
end

def player_move!(brd)
  square = ' '
  loop do
    prompt "Please choose a square. Remaining squares = #{empty_squares(brd)}"
    square = gets.chomp.to_i

    break if empty_squares(brd).include?(square)
    prompt 'That is not a valid entry. Please try again.'
  end
  brd[square] = PLAYER_MARKER
end

def computer_move!(brd)
  square = if !!offensive_move(brd)
             offensive_move(brd)
           elsif !!detect_threat(brd)
             detect_threat(brd)
           elsif empty_squares(brd).include?(5)
             5
           else
             empty_squares(brd).sample
           end
  brd[square] = COMPUTER_MARKER
end

def someone_won?(brd)
  !!detect_winner(brd)
end

def detect_winner(brd)
  WINNING_LINES.each do |line|
    if brd[line[0]] == PLAYER_MARKER &&
       brd[line[1]] == PLAYER_MARKER &&
       brd[line[2]] == PLAYER_MARKER
      return 'Player'
    elsif brd[line[0]] == COMPUTER_MARKER &&
          brd[line[1]] == COMPUTER_MARKER &&
          brd[line[2]] == COMPUTER_MARKER
      return 'Computer'
    end
  end
  nil
end

def detect_threat(brd)
  WINNING_LINES.each do |line|
    if (brd[line[0]] == PLAYER_MARKER &&
       brd[line[1]] == PLAYER_MARKER) &&
       empty_squares(brd).include?(line[2])
      return line[2]
    elsif (brd[line[0]] == PLAYER_MARKER &&
       brd[line[2]] == PLAYER_MARKER) &&
          empty_squares(brd).include?(line[1])
      return line[1]
    elsif (brd[line[2]] == PLAYER_MARKER &&
       brd[line[1]] == PLAYER_MARKER) &&
          empty_squares(brd).include?(line[0])
      return line[0]
    end
  end
  false
end

def offensive_move(brd)
  WINNING_LINES.each do |line|
    if (brd[line[0]] == COMPUTER_MARKER &&
       brd[line[1]] == COMPUTER_MARKER) &&
       empty_squares(brd).include?(line[2])
      return line[2]
    elsif (brd[line[0]] == COMPUTER_MARKER &&
       brd[line[2]] == COMPUTER_MARKER) &&
          empty_squares(brd).include?(line[1])
      return line[1]
    elsif (brd[line[2]] == COMPUTER_MARKER &&
       brd[line[1]] == COMPUTER_MARKER) &&
          empty_squares(brd).include?(line[0])
      return line[0]
    end
  end
  false
end

loop do
  initialize_board(board)

  loop do
    display_board(board)
    player_move!(board)
    break if someone_won?(board) || empty_squares(board).empty?

    computer_move!(board)
    break if someone_won?(board) || empty_squares(board).empty?
  end

  display_board(board)

  if someone_won?(board)
    prompt "#{detect_winner(board)} won!"
  else
    prompt "It's a tie!"
  end

  prompt 'Would you like to play again? (Y for yes)'
  play_again = gets.chomp.downcase

  break if play_again != 'y'
end
