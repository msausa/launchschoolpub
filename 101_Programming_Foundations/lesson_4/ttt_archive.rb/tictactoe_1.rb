# 1. Display the initial empty 3x3 board.
# 2. Ask the user to mark a square.
# 3. Computer marks a square.
# 4. Display the updated board state.
# 5. If winner, display winner.
# 6. If board is full, display tie.
# 7. If neither winner nor board is full, go to #2
# 8. Play again?
# 9. If yes, go to #1
# 10. Good bye!

load 'helper_methods.rb'
require 'pry'

def display_board(brd)
  clear_screen
  puts ""
  puts "     |     |     "
  puts "  #{brd[1]}  |  #{brd[2]}  |  #{brd[3]}  "
  puts "     |     |     "
  puts "-----+-----+-----"
  puts "     |     |     "
  puts "  #{brd[4]}  |  #{brd[5]}  |  #{brd[6]}  "
  puts "     |     |     "
  puts "-----+-----+-----"
  puts "     |     |     "
  puts "  #{brd[7]}  |  #{brd[8]}  |  #{brd[9]}  "
  puts "     |     |     "
  puts ""
end

def initialize_board
  new_board = {}
  (1..9).each {|num| new_board[num] = ' '}
  new_board
end

def player_choice_valid?(player_choice, choice_options)
  true if (1..9).include?(player_choice) && choice_options.include?(player_choice)
end

def out_of_moves?(choice_options)
  choice_options.empty?
end

#Will return true at initialization
def winner?(board)
  if (board[1] == board[2]) && (board[2] == board[3])
    puts "#{board[1]}'s win!!"
  elsif (board[4] == board[5]) && (board[5] == board[6])
    puts "#{board[4]}'s win!!"
  else
    puts "blah"
  end
end


loop do
  board = initialize_board
  display_board(board)

  choice_options = [1,2,3,4,5,6,7,8,9]

  loop do
    player_choice = ''
    loop do
      choice_prompt = <<-MSG
      You're X's! Please pick a square!
      1 = Top Left 2 = Top Middle 3 = Top Right
      4 = Middle Left 5 = Middle Middle 6 = Middle Right
      7 = Bottom Left 8 = Bottom Middle 9 = Bottom Right
      MSG

      prompt(choice_prompt)
      player_choice = gets.chomp.to_i

      if player_choice_valid?(player_choice, choice_options)
        break
      else
        prompt 'Entry is not valid. Please try again.'
      end
    end

    choice_options.delete(player_choice)
    board[player_choice] = "X"

    display_board(board)

    computer_choice = choice_options.sample
    choice_options.delete(computer_choice)

    board[computer_choice] = "O"

    display_board(board)

    if out_of_moves?(choice_options)
      prompt "No more moves. It's a draw!"
      break
    elsif winner?(board)
      prompt "Winner!"
      break
    end
  end
end
