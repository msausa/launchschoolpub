# 1. Display the initial empty 3x3 board.
# 2. Ask the user to mark a square.
# 3. Computer marks a square.
# 4. Display the updated board state.
# 5. If winner, display winner.
# 6. If board is full, display tie.
# 7. If neither winner nor board is full, go to #2
# 8. Play again?
# 9. If yes, go to #1
# 10. Good bye!

load 'helper_methods.rb'
require 'pry'

INITIAL_MARKER = ' '
PLAYER_MARKER = 'X'
COMPUTER_MARKER = 'O'
board = {}

def display_board(brd)
  clear_screen
  puts ""
  puts "     |     |     "
  puts "  #{brd[1]}  |  #{brd[2]}  |  #{brd[3]}  "
  puts "     |     |     "
  puts "-----+-----+-----"
  puts "     |     |     "
  puts "  #{brd[4]}  |  #{brd[5]}  |  #{brd[6]}  "
  puts "     |     |     "
  puts "-----+-----+-----"
  puts "     |     |     "
  puts "  #{brd[7]}  |  #{brd[8]}  |  #{brd[9]}  "
  puts "     |     |     "
  puts ""
end

def initialize_board(brd)
  (1..9).each { |num| brd[num] = INITIAL_MARKER }
end

def empty_squares(brd)
  brd.keys.select { |num| brd[num] == INITIAL_MARKER }
end

def player_move(brd)
  square = ' '
  loop do
    prompt "Please choose a square. Remaining squares = #{empty_squares(brd)}"
    square = gets.chomp.to_i

    break if empty_squares(brd).include?(square)
    prompt 'That is not a valid entry. Please try again.'
  end
  brd[square] = PLAYER_MARKER
  display_board(brd)
end

def computer_move(brd)
  square = empty_squares(brd).sample
  brd[square] = COMPUTER_MARKER
  display_board(brd)
end

def someone_won?(rows)
  rows.each do |row|
    if row.uniq.count == 1 && row.uniq.first != INITIAL_MARKER
      puts "#{row.uniq.first}'s wins!!"
      return true
    end
  end
  return false
end

#I think there is a better way to do this but not sure exactly what it is yet.

def rows(brd)
  [["#{brd[1]}", "#{brd[2]}", "#{brd[3]}"],
   ["#{brd[4]}", "#{brd[5]}", "#{brd[6]}"],
   ["#{brd[7]}", "#{brd[8]}", "#{brd[9]}"],
   ["#{brd[1]}", "#{brd[4]}", "#{brd[7]}"],
   ["#{brd[2]}", "#{brd[5]}", "#{brd[8]}"],
   ["#{brd[3]}", "#{brd[6]}", "#{brd[9]}"],
   ["#{brd[1]}", "#{brd[5]}", "#{brd[9]}"],
   ["#{brd[7]}", "#{brd[5]}", "#{brd[3]}"]]
end

loop do
  initialize_board(board)

  loop do

    display_board(board)

    player_move(board)

    computer_move(board)

    break if someone_won?(rows(board)) || empty_squares(board).empty?

  end
  prompt 'Would you like to play again? (Y for yes)'
  play_again = gets.chomp.downcase

  break if play_again != 'y'
end
