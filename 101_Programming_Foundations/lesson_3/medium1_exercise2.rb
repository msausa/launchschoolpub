# statement = "The Flintstones Rock"
#
# hash = {}
#
# letters = statement.scan(/[[:alpha:]]/)
# letters.each do |letter|
#   letter_frequency = statement.scan(letter).count
#   hash[letter] = letter_frequency
# end
#
# p hash

statement = "The Flintstones Rock"

char_frequency = statement.scan(/[[:alpha:]]/).each_with_object(Hash.new(0)) {|item, hash| hash[item] += 1}

puts char_frequency
