# require 'pry'

munsters = {
  "Herman" => { "age" => 32, "gender" => "male" },
  "Lily" => { "age" => 30, "gender" => "female" },
  "Grandpa" => { "age" => 402, "gender" => "male" },
  "Eddie" => { "age" => 10, "gender" => "male" },
  "Marilyn" => { "age" => 23, "gender" => "female"}
}

# First Attempt
# munsters.each do |key, value|
#   if (0..17).cover?(munsters[key]["age"])
#     munsters[key]["age_group"] = "kid"
#   elsif (18..64).cover?(munsters[key]["age"])
#     munsters[key]["age_group"] = "adult"
#   else
#     munsters[key]["age_group"] = "senior"
#   end
# end

#Second Attempt
# munsters.each do |key, value|
#   case
#   when (0..17).cover?(munsters[key]["age"])
#     munsters[key]["age_group"] = "kid"
#   when (18..64).cover?(munsters[key]["age"])
#     munsters[key]["age_group"] = "adult"
#   else
#     munsters[key]["age_group"] = "senior"
#   end
# end

#Suggest Solution (WAAAAY Better/More Simple)
munsters.each do |name, info|
  case info["age"]
  when 0...18
    info["age_group"] = "kid"
  when 18...65
    info["age_group"] = "adult"
  else
    info["age_group"] = "senior"
  end
end

puts munsters
