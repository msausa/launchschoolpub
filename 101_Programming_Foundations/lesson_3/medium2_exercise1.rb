munsters = {
  "Herman" => { "age" => 32, "gender" => "male" },
  "Lily" => { "age" => 30, "gender" => "female" },
  "Grandpa" => { "age" => 402, "gender" => "male" },
  "Eddie" => { "age" => 10, "gender" => "male" }
}

#Figure out the total age of just the male members of the family.
total_age = 0
munsters.each do |_, info|
  total_age += info['age'] if info['gender'] == 'male'
end

puts total_age

#Could also be represented as:
# if info["gender"] == "male"
#   total_age += info["age"]
# end
