require 'SecureRandom'
def generate_uuid_secure_random
  lengths = [4, 2, 2, 2, 2, 6]

  uuid = []
  lengths.each do |len|
    uuid << SecureRandom.hex(len)
  end
  uuid.join("-")
end

def generate_uuid
  lengths = [8, 4, 4, 4, 4, 12]

  uuid = []
  lengths.each do |len|
    uuid << [*('a'..'z'),*('0'..'9')].shuffle[0,len].join
  end
  uuid.join("-")
end
